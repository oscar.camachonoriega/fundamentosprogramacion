#include <iostream>
#include <stdio.h>

using namespace std;

void gotoxy(int x, int y) {
    printf("\x1b[%d;%df", y, x);
}


int main()
{
	system("clear");
	
	for (int i = 0; i < 20; ++i)
	{
		/* code */
		gotoxy(10, i);
		printf("*");

		gotoxy(30, i);
		printf("*");


	}

	for (int i = 0; i < 50; ++i)
	{
		/* code */
		gotoxy(i, 5);
		printf("*");

		gotoxy(i, 15);
		printf("*");
	}


	gotoxy(5, 10);
	printf("X");

	gotoxy(20, 10);
	printf("O");
	//system("PAUSE()");
	getchar();
	return 0;
}
